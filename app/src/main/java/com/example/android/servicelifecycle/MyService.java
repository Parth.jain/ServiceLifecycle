package com.example.android.servicelifecycle;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by parth on 3/1/18.
 */

class MyService extends Service{
    private static final String TAG="In Service Method";
    IBinder mBinder;
    int mStartMode;
    boolean mAllowedRebind;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG,"Service Created");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        Log.i(TAG,"Service is Started");

        return START_STICKY;
    }

    //@Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG,"Service Binded");

        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG,"service unbinding");
        return mAllowedRebind;

    }

    @Override
    public void onRebind(Intent intent) {
        Log.i(TAG,"service rebind started");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"service destroyed");
    }
}
