package com.example.android.servicelifecycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button startBtn,stopBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startBtn=findViewById(R.id.start);
        startBtn.setOnClickListener(this);
        stopBtn=findViewById(R.id.stop);
        stopBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
    if(startBtn==v)
    {
        Log.i("MainActivity Method","Starting the service");
        Intent serviceInt=new Intent(this,MyService.class);
        startService(serviceInt);
    }
    else{
        Log.i("stop pressed","stop!");
        Intent in=new Intent(this,MyService.class);
        in.setAction("stop");
        stopService(in);
    }
    }
}
